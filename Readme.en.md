# Questions Extension for TYPO3

Display and manage FAQ questions - SEO optimized.


**Features:**

*   Easy to use
*   SEO optimized integration
*   Microdata (http://schema.org/Question) prepared
*   Listing with search word and category filter
*   Markdown support for answer text (requires EXT:parsedown_extra)
*   Create Product related FAQ for EXT:shop
*   Additional fields for custom information
*   Backend module for question management
*   CSV-Export for records in backend
*   Can also be easily integrated into an intranet to display recurring questions

If you need some additional or custom feature - get in contact!


**Links:**

*   [Questions Documentation](https://www.coding.ms/documentation/typo3-questions "Questions Documentation")
*   [Questions Bug-Tracker](https://gitlab.com/codingms/typo3-public/questions/-/issues "Questions Bug-Tracker")
*   [Questions Repository](https://gitlab.com/codingms/typo3-public/questions "Questions Repository")
*   [TYPO3 Questions Productdetails](https://www.coding.ms/typo3-extensions/typo3-questions/ "TYPO3 Questions Productdetails")
*   [TYPO3 Questions Documentation](https://www.coding.ms/documentation/typo3-questions "TYPO3 Questions Documentation")
*   [TYPO3 Questions Download](https://typo3.org/extensions/repository/view/questions "TYPO3 Questions Download")
*   [TYPO3 Parsedown-Extra Productdetails](https://www.coding.ms/typo3-extensions/typo3-parsedown-extra/ "TYPO3 Parsedown-Extra Productdetails")
*   [TYPO3 Shop Productdetails](https://www.coding.ms/typo3-extensions/typo3-shop/ "TYPO3 Shop Productdetails")
