<?php

declare(strict_types=1);

namespace CodingMs\Questions\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Tca\Configuration as ConfigurationDefaults;

/**
 * Configuration presets for TCA fields.
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class Configuration extends ConfigurationDefaults
{
    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label Additional label, e.g. checkboxes.
     * @param array<string, mixed> $options Options for select boxes, field name for slug
     * @return array<string, mixed>
     */
    public static function get(
        string $type,
        bool $required = false,
        bool $readonly = false,
        string $label = '',
        array $options=[]
    ): array {
        switch ($type) {
            case 'questionCategories':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectMultipleSideBySide',
                    'foreign_table' => 'tx_questions_domain_model_questioncategory',
                    'MM' => 'tx_questions_question_questioncategory_mm',
                    'foreign_table_where' => 'AND tx_questions_domain_model_questioncategory.pid=###CURRENT_PID### AND tx_questions_domain_model_questioncategory.sys_language_uid IN (-1,0) ORDER BY title',
                    'size' => 10,
                    'autoSizeMax' => 30,
                    'minitems' => 1,
                    'maxitems' => 9999,
                    'multiple' => 0,
                    'wizards' => [
                        '_PADDING' => 1,
                        '_VERTICAL' => 1,
                        'edit' => [
                            'module' => [
                                'name' => 'wizard_edit',
                            ],
                            'type' => 'popup',
                            'title' => 'Edit',
                            'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
                            'popup_onlyOpenIfSelected' => 1,
                            'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                        ],
                        'add' => [
                            'module' => [
                                'name' => 'wizard_add',
                            ],
                            'type' => 'script',
                            'title' => 'Create new',
                            'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                            'params' => [
                                'table' => 'tx_questions_domain_model_questioncategory',
                                'pid' => '###CURRENT_PID###',
                                'setValue' => 'prepend'
                            ],
                        ],
                    ],
                ];
                break;
            default:
                $config = parent::get($type, $required, $readonly, $label, $options);
                break;
        }
        if ($readonly) {
            $config['readOnly'] = true;
        }
        return $config;
    }
}
