<?php

namespace CodingMs\Questions\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Modules\Utility\AccessUtility as AccessUtilityParent;
use CodingMs\Questions\Domain\DataTransferObject\QuestionActionPermission;
use CodingMs\Questions\Domain\DataTransferObject\QuestionCategoryActionPermission;

/**
 * Utility: Access Permissions for Backend User Authentication
 */
class AccessUtility extends AccessUtilityParent
{
    /**
     * Check if user can add table
     *
     * @param  string  $table
     * @return bool
     */
    public static function beUserHasRightToAddTable(string $table): bool
    {
        $return = true;
        if (self::beUserHasRightToEditTable($table)) {
            switch ($table) {
                case 'tx_questions_domain_model_question':
                    $return = QuestionActionPermission::isConfigured(QuestionActionPermission::ACTION_ADD_QUESTION);
                    break;
                case 'tx_questions_domain_model_questioncategory':
                    $return = QuestionCategoryActionPermission::isConfigured(QuestionCategoryActionPermission::ACTION_ADD_CATEGORY);
                    break;
            }
        }
        return $return;
    }

    /**
     * Check if user can delete table
     *
     * @param  string  $table
     * @return bool
     */
    public static function beUserHasRightToDeleteTable(string $table): bool
    {
        $return = true;
        switch ($table) {
            case 'tx_questions_domain_model_question':
                $return = QuestionActionPermission::isConfigured(QuestionActionPermission::ACTION_DELETE_QUESTION);
                break;
            case 'tx_questions_domain_model_questioncategory':
                $return = QuestionCategoryActionPermission::isConfigured(QuestionCategoryActionPermission::ACTION_DELETE_CATEGORY);
                break;
        }
        return $return;
    }
}
