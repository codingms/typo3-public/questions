<?php

namespace CodingMs\Questions\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Modules\Controller\BackendController as BackendBaseController;
use CodingMs\Modules\Utility\BackendListUtility;
use CodingMs\Questions\Domain\DataTransferObject\QuestionActionPermission;
use CodingMs\Questions\Domain\DataTransferObject\QuestionCategoryActionPermission;
use CodingMs\Questions\Domain\Model\QuestionCategory;
use CodingMs\Questions\Domain\Repository\QuestionCategoryRepository;
use CodingMs\Questions\Domain\Repository\QuestionRepository;
use Exception;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Routing\UriBuilder as UriBuilderBackend;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * BackendController
 */
class BackendController extends BackendBaseController
{
    protected QuestionRepository $questionRepository;
    protected QuestionCategoryRepository $questionCategoryRepository;

    public function __construct(
        TypoScriptService $typoScriptService,
        BackendListUtility $backendListUtility,
        UriBuilderBackend $uriBuilderBackend,
        ModuleTemplateFactory $moduleTemplateFactory,
        QuestionRepository $questionRepository,
        QuestionCategoryRepository $questionCategoryRepository
    ) {
        parent::__construct(
            $typoScriptService,
            $backendListUtility,
            $uriBuilderBackend,
            $moduleTemplateFactory
        );
        //
        $this->moduleName = 'questions_questions';
        $this->modulePrefix = 'tx_questions_questionsquestions';
        if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
            $this->moduleName = 'web_QuestionsQuestions';
            $this->modulePrefix = 'tx_questions_web_questionsquestions';
        }
        $this->extensionName = 'Questions';
        //
        $this->questionRepository = $questionRepository;
        $this->questionCategoryRepository = $questionCategoryRepository;
    }

    protected function initializeAction()
    {
        parent::initializeAction();
        $this->createMenu();
        $this->createButtons();
    }

    /**
     * Create action menu
     */
    protected function createMenu(): void
    {
        $actions = [
            [
                'action' => 'overviewQuestions',
                'label' => LocalizationUtility::translate('tx_questions_label.menu_overview_questions', $this->extensionName)
            ],
        ];
        if (QuestionCategoryActionPermission::listCategoriesAllowed()) {
            $actions[] = [
                'action' => 'overviewCategories',
                'label' => LocalizationUtility::translate('tx_questions_label.menu_overview_categories', $this->extensionName)
            ];
        } elseif (!QuestionCategoryActionPermission::listCategoriesAllowed() && isset($_SERVER['DDEV_HOSTNAME'])) {
            $actions[] = [
                'action' => 'overviewCategories',
                'label' => LocalizationUtility::translate('tx_questions_label.menu_overview_categories', $this->extensionName) . ' [disallowed]'
            ];
        }
        $this->createMenuActions($actions);
    }

    /**
     * Add menu buttons for specific actions
     *
     * @throws Exception
     */
    protected function createButtons(): void
    {
        $buttonBar = $this->moduleTemplate->getDocHeaderComponent()->getButtonBar();
        switch ($this->request->getControllerActionName()) {
            case 'overviewQuestions':
                // New
                $this->getButton(
                    $buttonBar,
                    'new',
                    [
                        'translationKey' => 'new_question',
                        'table' => 'tx_questions_domain_model_question'
                    ],
                    !QuestionActionPermission::questionCreationAllowed()
                );
                // CSV export
                $this->getButton($buttonBar, 'csv', [
                    'translationKey' => 'export_questions',
                    'action' => 'overviewQuestions',
                    'controller' => 'Backend',
                ]);
                //
                $this->getButton($buttonBar, 'refresh', [
                    'translationKey' => 'list_overview_questions_refresh'
                ]);
                $this->getButton($buttonBar, 'bookmark', [
                    'translationKey' => 'list_overview_questions_bookmark'
                ]);
                break;
            case 'overviewCategories':
                // New
                $this->getButton(
                    $buttonBar,
                    'new',
                    [
                    'translationKey' => 'new_category',
                    'table' => 'tx_questions_domain_model_questioncategory'
                ],
                    !QuestionCategoryActionPermission::categoryCreationAllowed()
                );
                // CSV export
                $this->getButton($buttonBar, 'csv', [
                    'translationKey' => 'export_categories',
                    'action' => 'overviewCategories',
                    'controller' => 'Backend',
                ]);
                //
                $this->getButton($buttonBar, 'refresh', [
                    'translationKey' => 'list_overview_categories_refresh'
                ]);
                $this->getButton($buttonBar, 'bookmark', [
                    'translationKey' => 'list_overview_categories_bookmark'
                ]);
                break;
        }
    }

    /**
     * Questions overview
     *
     * @throws NoSuchArgumentException
     * @throws InvalidQueryException
     */
    public function overviewQuestionsAction(): ResponseInterface
    {
        // Build list
        $list = $this->backendListUtility->initList(
            $this->settings['lists']['questions'],
            $this->request,
            ['category']
        );
        // Initialize filtering
        $list['category']['items'] = [];
        /** @var QuestionCategory $category */
        foreach ($this->questionCategoryRepository->findAll() as $category) {
            $list['category']['items'][$category->getUid()] = $category->getTitle();
        }
        if (count($list['category']['items']) === 0) {
            $this->addFlashMessage(
                'Please add at least one question category',
                'Error',
                AbstractMessage::ERROR
            );
        }
        // Selected category
        if ($this->request->hasArgument('category')) {
            $list['category']['selected'] = (int)$this->request->getArgument('category');
        }
        // Allow delete?!
        if (!QuestionActionPermission::questionDeletionAllowed()) {
            if ($list['authorization']['isDdev']) {
                $list['actions']['delete']['css'] = 'bg-danger';
            } else {
                $list['actions']['delete']['css'] = 'disabled';
            }
        }
        // Store settings
        $this->backendListUtility->writeSettings($list['id'], $list);
        // Export Result as CSV!?
        if ($this->request->hasArgument('csv')) {
            $list['limit'] = 0;
            $list['offset'] = 0;
            $list['pid'] = $this->pageUid;
            $questions = $this->questionRepository->findAllForBackendList($list);
            $list['countAll'] = $this->questionRepository->findAllForBackendList($list, true);
            $this->backendListUtility->exportAsCsv($questions, $list);
        } else {
            $list['pid'] = $this->pageUid;
            $questions = $this->questionRepository->findAllForBackendList($list);
            $list['countAll'] = $this->questionRepository->findAllForBackendList($list, true);
        }
        $this->getViewToUse()->assign('list', $list);
        $this->getViewToUse()->assign('questions', $questions);
        $this->getViewToUse()->assign('currentPage', $this->pageUid);
        $this->getViewToUse()->assign('actionMethodName', $this->actionMethodName);
        return $this->renderViewToUse();
    }

    /**
     * Categories overview
     *
     * @throws NoSuchArgumentException
     * @throws InvalidQueryException
     */
    public function overviewCategoriesAction(): ResponseInterface
    {
        if (!QuestionCategoryActionPermission::listCategoriesAllowed()) {
            $this->redirect('overviewQuestions');
        }
        // Build list
        $list = $this->backendListUtility->initList(
            $this->settings['lists']['categories'],
            $this->request
        );
        // Allow delete?!
        if (!QuestionCategoryActionPermission::categoryDeletionAllowed()) {
            if ($list['authorization']['isDdev']) {
                $list['actions']['delete']['css'] = 'bg-danger';
            } else {
                $list['actions']['delete']['css'] = 'disabled';
            }
        }
        // Store settings
        $this->backendListUtility->writeSettings($list['id'], $list);
        // Export Result as CSV!?
        if ($this->request->hasArgument('csv')) {
            $list['limit'] = 0;
            $list['offset'] = 0;
            $list['pid'] = $this->pageUid;
            $categories = $this->questionCategoryRepository->findAllForBackendList($list);
            $list['countAll'] = $this->questionCategoryRepository->findAllForBackendList($list, true);
            $this->backendListUtility->exportAsCsv($categories, $list);
        } else {
            $list['pid'] = $this->pageUid;
            $categories = $this->questionCategoryRepository->findAllForBackendList($list);
            $list['countAll'] = $this->questionCategoryRepository->findAllForBackendList($list, true);
        }
        $this->getViewToUse()->assign('list', $list);
        $this->getViewToUse()->assign('categories', $categories);
        $this->getViewToUse()->assign('currentPage', $this->pageUid);
        $this->getViewToUse()->assign('actionMethodName', $this->actionMethodName);
        return $this->renderViewToUse();
    }
}
