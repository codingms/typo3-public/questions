<?php

namespace CodingMs\Questions\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Questions\Domain\Model\Question;
use CodingMs\Questions\Domain\Model\QuestionCategory;
use CodingMs\Questions\Domain\Repository\QuestionCategoryRepository;
use CodingMs\Questions\Domain\Repository\QuestionRepository;
use CodingMs\Questions\PageTitle\PageTitleProvider;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerInterface;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * QuestionController
 */
class QuestionController extends ActionController
{
    protected QuestionRepository $questionRepository;
    protected QuestionCategoryRepository $questionCategoryRepository;

    /**
     * @param QuestionRepository $questionRepository
     * @param QuestionCategoryRepository $questionCategoryRepository
     */
    public function __construct(
        QuestionRepository $questionRepository,
        QuestionCategoryRepository $questionCategoryRepository
    ) {
        $this->questionRepository = $questionRepository;
        $this->questionCategoryRepository = $questionCategoryRepository;
    }

    /**
     * @throws InvalidQueryException
     */
    public function listAction(): ResponseInterface
    {
        if((int)VersionNumberUtility::getCurrentTypo3Version() > 11) {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->request->getAttribute('currentContentObject');
        } else {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->configurationManager->getContentObject();
        }
        if (!isset($this->settings['list'])) {
            $this->addFlashMessage(
                'Please insert static template of question extension.',
                'Error!',
                AbstractMessage::ERROR
            );
        }
        $questionCategories = [];
        $categoryUids = GeneralUtility::trimExplode(
            ',',
            $this->settings['list']['categories'],
            true
        );
        if (count($categoryUids) > 0) {
            foreach ($categoryUids as $categoryUid) {
                $category = $this->questionCategoryRepository->findByIdentifier($categoryUid);
                if ($category instanceof QuestionCategory) {
                    $questionCategories[] = $category;
                }
            }
        } else {
            $questionCategories = $this->questionCategoryRepository->findAll()->toArray();
        }
        $questions = $this->questionRepository->findByFilter($questionCategories, $this->settings);
        $this->view->assign('questions', $questions);
        $this->view->assign('questionCategories', $questionCategories);
        $this->view->assign('contentObject', $contentObject->data);
        return $this->htmlResponse();
    }

    /**
     * @throws NoSuchArgumentException
     * @throws StopActionException
     */
    public function showAction(): ResponseInterface
    {
        if((int)VersionNumberUtility::getCurrentTypo3Version() > 11) {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->request->getAttribute('currentContentObject');
        } else {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->configurationManager->getContentObject();
        }
        /** @var Question $question */
        $question = null;
        $questionUid = 'undefined';
        if ($this->request->hasArgument('question')) {
            $questionUid = (int)$this->request->getArgument('question');
            $question = $this->questionRepository->findByIdentifier($questionUid);
        }
        if (!($question instanceof question)) {
            // Question not found?!
            // Redirect to list view
            $this->addFlashMessage(
                'Question not found (uid: ' . $questionUid . ')!',
                'Error',
                AbstractMessage::ERROR
            );
            $this->redirect('list');
        }
        //
        // Page title and meta tags
        /** @var MetaTagManagerRegistry $metaTagManager */
        $metaTagManagerRegistry = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);
        /** @var MetaTagManagerInterface $metaTagManagerDescription */
        $metaTagManagerDescription = $metaTagManagerRegistry->getManagerForProperty('description');
        $metaTagManagerDescription->addProperty('description', $question->getMetaDescription());
        /** @var MetaTagManagerInterface $metaTagManagerAbstract */
        $metaTagManagerAbstract = $metaTagManagerRegistry->getManagerForProperty('abstract');
        $metaTagManagerAbstract->addProperty('abstract', $question->getMetaAbstract());
        /** @var MetaTagManagerInterface $metaTagManagerKeywords */
        $metaTagManagerKeywords = $metaTagManagerRegistry->getManagerForProperty('keywords');
        $metaTagManagerKeywords->addProperty('keywords', $question->getMetaKeywords());
        /** @var PageTitleProvider $seoTitlePageTitleProvider */
        $seoTitlePageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
        $seoTitlePageTitleProvider->setTitle($question->getHtmlTitle());
        //
        $this->view->assign('question', $question);
        $this->view->assign('contentObject', $contentObject->data);
        return $this->htmlResponse();
    }
}
