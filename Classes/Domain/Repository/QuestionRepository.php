<?php

namespace CodingMs\Questions\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Questions\Domain\Model\QuestionCategory;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Question repository
 */
class QuestionRepository extends Repository
{
    /**
     * @param QuestionCategory $category
     * @return array|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findByCategoryGlobal($category)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching(
            $query->contains('category', $category)
        );
        $query->setOrderings(['sorting' => QueryInterface::ORDER_ASCENDING]);
        return $query->execute();
    }

    /**
     * @param array $categories
     * @param array $settings
     * @return array|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findByFilter($categories = [], $settings = [])
    {
        $query = $this->createQuery();
        if (!empty($categories)) {
            $conditions = [];
            foreach ($categories as $category) {
                $conditions[] = $query->contains('category', $category);
            }
            if (count($conditions) === 1) {
                $query->matching(
                    $conditions[0]
                );
            } else {
                $query->matching(
                    $query->logicalOr(...$conditions)
                );
            }
        }
        if ($settings['list']['sortOrder'] === 'asc') {
            $query->setOrderings([$settings['list']['sortBy'] => QueryInterface::ORDER_ASCENDING]);
        } else {
            $query->setOrderings([$settings['list']['sortBy'] => QueryInterface::ORDER_DESCENDING]);
        }
        if (!$settings['list']['pagination'] && (int)$settings['list']['limit'] > 0) {
            $query->setLimit((int)$settings['list']['limit']);
        }
        return $query->execute();
    }

    /**
     * @param array $filter
     * @param bool $count
     * @return array|int|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findAllForBackendList(array $filter = [], $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        // category filter
        $constraints = [];
        if (isset($filter['category']['selected']) && $filter['category']['selected'] > 0) {
            $constraints[] = $query->contains('category', $filter['category']['selected']);
        }
        if (count($constraints) === 1) {
            $query->matching($constraints[0]);
        }
        // end category filter
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '') {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }
}
