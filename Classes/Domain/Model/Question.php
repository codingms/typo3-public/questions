<?php

namespace CodingMs\Questions\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute1Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute2Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute3Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\Attribute4Trait;
use CodingMs\AdditionalTca\Domain\Model\Traits\CreationDateTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\HiddenTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\HtmlTitleTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MarkdownTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaAbstractTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaDescriptionTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\MetaKeywordsTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\ModificationDateTrait;
use CodingMs\Modules\Domain\Model\Traits\CheckMethodTrait;
use CodingMs\Modules\Domain\Model\Traits\ToCsvArrayTrait;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Question
 */
class Question extends AbstractEntity
{
    use CheckMethodTrait;
    use ToCsvArrayTrait;
    use CreationDateTrait;
    use ModificationDateTrait;
    use HiddenTrait;
    use HtmlTitleTrait;
    use MetaDescriptionTrait;
    use MetaAbstractTrait;
    use MetaKeywordsTrait;
    use Attribute1Trait;
    use Attribute2Trait;
    use Attribute3Trait;
    use Attribute4Trait;
    use MarkdownTrait;

    /**
     * @var string
     */
    protected $question = '';

    /**
     * @var string
     */
    protected $answer = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Questions\Domain\Model\QuestionCategory>
     */
    protected $category;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $images;

    /**
     * @var string
     */
    protected $template = '';

    public function __construct()
    {
        $this->images = new ObjectStorage();
        $this->category = new ObjectStorage();
    }

    /**
     * Returns the question
     *
     * @return string $question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Sets the question
     *
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * Returns the answer
     *
     * @return string $answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Sets the answer
     *
     * @param string $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * Adds a QuestionCategory
     *
     * @param \CodingMs\Questions\Domain\Model\QuestionCategory $category
     */
    public function addCategory(QuestionCategory $category)
    {
        $this->category->attach($category);
    }

    /**
     * Removes a QuestionCategory
     *
     * @param \CodingMs\Questions\Domain\Model\QuestionCategory $categoryToRemove The QuestionCategory to be removed
     */
    public function removeCategory(QuestionCategory $categoryToRemove)
    {
        $this->category->detach($categoryToRemove);
    }

    /**
     * Returns the categories
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Questions\Domain\Model\QuestionCategory> $category
     */
    public function getCategories()
    {
        return $this->category;
    }

    /**
     * Sets the categories
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Questions\Domain\Model\QuestionCategory> $categories
     */
    public function setCategories(ObjectStorage $categories)
    {
        $this->category = $categories;
    }

    /**
     * Returns the category uids as a comma separated string
     *
     * @return string
     */
    public function getCategoryUidsCommaSeparated()
    {
        $uids = [];
        /** @var QuestionCategory $category */
        foreach ($this->getCategories() as $category) {
            $uids[] = $category->getUid();
        }
        return implode(',', $uids);
    }

    /**
     * Returns the category uids as a comma separated string
     *
     * @return string
     */
    public function getCategoryTitlesCommaSeparated()
    {
        $titles = [];
        /** @var QuestionCategory $category */
        foreach ($this->getCategories() as $category) {
            $titles[] = $category->getTitle();
        }
        return implode(', ', $titles);
    }

    /**
     * Adds a image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function addImage(FileReference $image)
    {
        $this->images->attach($image);
    }

    /**
     * Removes a image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The image to be removed
     */
    public function removeImage(FileReference $imageToRemove)
    {
        $this->images->detach($imageToRemove);
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     */
    public function setImages(ObjectStorage $images)
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }
}
