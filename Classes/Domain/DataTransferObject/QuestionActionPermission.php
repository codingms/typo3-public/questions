<?php

namespace CodingMs\Questions\Domain\DataTransferObject;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Modules\Domain\DataTransferObject\AbstractPermission;
use CodingMs\Modules\Domain\DataTransferObject\PermissionTrait;

/**
 * DTO: Permission access Question Actions
 */
final class QuestionActionPermission extends AbstractPermission
{
    use PermissionTrait;

    /**
     * @var string
     */
    public const KEY = 'modules_question';

    /**
     * Disabled actions
     */
    public const ACTION_ADD_QUESTION = 'ACTION_ADD_QUESTION';
    public const ACTION_DELETE_QUESTION = 'ACTION_DELETE_QUESTION';

    /**
     * @return array<string, mixed>
     */
    public static function getItems(): array
    {
        return [
            self::ACTION_ADD_QUESTION => [
                'LLL:EXT:questions/Resources/Private/Language/locallang.xlf:tx_questions_authorization.question_add_question_title',
                'module-questions',
            ],
            self::ACTION_DELETE_QUESTION => [
                'LLL:EXT:questions/Resources/Private/Language/locallang.xlf:tx_questions_authorization.question_delete_question_title',
                'module-questions',
            ],
        ];
    }

    protected function populateData(): void
    {
        $this->data = [
            'header' => 'LLL:EXT:questions/Resources/Private/Language/locallang.xlf:tx_questions_authorization.question_header',
            'items' => self::getItems(),
        ];
    }

    /** Helper classes for readability */

    /**
     * @return bool
     */
    public static function questionCreationAllowed(): bool
    {
        return static::isConfigured(self::ACTION_ADD_QUESTION);
    }

    /**
     * @return bool
     */
    public static function questionDeletionAllowed(): bool
    {
        return static::isConfigured(self::ACTION_DELETE_QUESTION);
    }
}
