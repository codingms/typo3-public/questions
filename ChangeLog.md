# Questions Change-Log

*	[TASK] Migrate TypoScript imports



## 2024-07-23 Release of version 4.0.11

*	[BUGFIX] Fix creating translated record container



## 2024-07-23 Release of version 4.0.10

*	[BUGFIX] Fix creating translated record container



## 2024-07-23 Release of version 4.0.9

*	[BUGFIX] Fix creating translated record container
*	[BUGFIX] Fix path for page icon



## 2024-04-22 Release of version 4.0.8

*	[BUGFIX] Fix allowed records on page type in TYPO3 12
*	[TASK] Backend filter migration



## 2024-01-26 Release of version 4.0.7

*	[BUGFIX] Fix repository injection for fetching category questions



## 2024-01-26 Release of version 4.0.6

*	[TASK] Add TCA description in shop product integration
*	[BUGFIX] Fix backend module authorization in TYPO3 12
*	[TASK] Optimize version conditions in PHP code
*	[TASK] Migrate controller modulePrefix initialization in backend module to TYPO3 12
*	[TASK] Optimize labels in user authorizations
*	[BUGFIX] Add enableNamespacedArgumentsForBackend feature for fixing return links in backend



## 2023-11-01 Release of version 4.0.5

*	[TASK] Clean up documentation
*	[BUGFIX] Fix get content object for TYPO3 11



## 2023-10-16  Release of version 4.0.4

*	[BUGFIX] Clean up backend module icon and registration



## 2023-10-04 Release of version 4.0.3

*	[BUGFIX] Fix invalid TypoScript setting



## 2023-10-03 Release of version 4.0.2

*	[TASK] Optimize documentation articles
*	[TASK] Migrate TCA for TYPO3 11 and 12 and optimize TypoScript constants groups



## 2023-09-04 Release of version 4.0.1

*	[TASK] Optimize documentation



## 2023-08-15  Release of version 4.0.0

*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10
*	[TASK] Add documentation links



## 2023-02-05  Release of version 3.2.1

*	[TASK] Add documentation for TypoScript constants
*	[TASK] CSH language files migration
*	[BUGFIX] Remove record types from shop integration



## 2022-11-20  Release of version 3.2.0

*	[FEATURE] Define access utility for backend management
*	[TASK] Add meta text to config.json
*	[TASK] Replace EditInModal by Edit in backend module
*	[BUGFIX] Fix documentation entry



## 2022-08-05  Release of version 3.1.0

*	[FEATURE] Add authorizations for backend user
*	[TASK] Migrate page.contains to page.type
*	[TASK] Migrate backend module header and footer



## 2022-05-09  Release of version 3.0.2

*	[BUGFIX] Fix localization parent in TCA



## 2022-04-09  Release of version 3.0.1

*	[TASK] Move backend folder icon into Resources/Public/Icons
*	[TASK] Clean up Fluid layout and backend controller
*	[TASK] Sync translated DE .xlf files
*	[TASK] Add translated DE .xlf files


## 2021-11-19  Release of version 3.0.0

*	[BUGFIX] Fix TCA override for shop extension
*	[TASK] Migration for TYPO3 11.5 - remove support for TYPO3 9.5
*	[BUGFIX] Fix documentation configuration
*	[TASK] Add documentation for sitemap xml configuration
*	[TASK] Add documentations configuration



## 2021-11-19  Release of version 2.1.7

*	[TASK] Add another sorting option for sorting field



## 2021-02-01  Release of version 2.1.6

*	[TASK] Remove NOT NULL from markdown database field



## 2021-01-07  Release of version 2.1.5

*	[TASK] Add more german translation



## 2020-10-11  Release of version 2.1.4

*	[BUGFIX] Fix extra tags in composer.json



## 2020-10-11  Release of version 2.1.3

*	[BUGFIX] Fix extra tags in composer.json



## 2020-10-11  Release of version 2.1.2

*	[TASK] Add filter field for selection in shop products



## 2020-07-16  Release of version 2.1.1

*	[BUGFIX] Fix slug field in TCA
*	[BUGFIX] Fix information row in FlexForm



## 2020-07-01  Release of version 2.1.0

*	[FEATURE] Integrate additional TCA extension usage



## 2020-06-30  Release of version 2.0.1

*	[BUGFIX] Fix rendering TYPO3 links in answer markup text
*	[TASK] Reformat ChangeLog content
*	[TASK] Add extra tags in composer.json



## 2020-03-26  Release of version 2.0.0

*	[TASK] Migration for TYPO3 9.5-10.4
*	[FEATURE] Backend filter for question category
*	[TASK] TCA refactoring
*	[TASK] Documentation
*	[TASK] Migrate Parsedown ViewHelper for TYPO3 9.5
*	[TASK] Optimize TCA.
*	[BUGFIX] Add missing backend labels.



## 2019-10-16  Release of version 1.2.0

*	[FEATURE] Add backend module for managing records.
*	[FEATURE] Add preview for plugins in backend page module.



## 2019-09-20  Release of version 1.1.2

*	[TASK] Rename TypoScript file extensions.
*	[TASK] Reconfigure Gitlab-CI configuration.



## 2019-03-16  Release of version 1.1.1

*	[TASK] Adding Gitlab-CI configuration.
*	[TASK] Remove inject annotations.



## 2019-03-01  Release of version 1.1.0

*	[BUGFIX] Removing DEV identifier.
*	[TASK] Removing unused use statements.
*	[TASK] Removing coding.ms when customer has already supported us.
*	[FEATURE] Adding sort order and sort by for questions list.
*	[FEATURE] Adding content object into Fluid template.
*	[TASK] Removing Realurl auto configuration.
*	[BUGFIX] Fixing sorting order in repository.
*	[BUGFIX] Fixing TypoScript Fluid template paths.



## 2017-11-24  Release of version 1.0.1

*	[BUGFIX] Fixing suggested extensions



## 2017-11-24  Release of version 1.0.0

*	[FEATURE] Adding Markdown support for answers
*	[TASK] Adding alternative titles in TCA
*	[TASK] Remove exclude fields from configuration
