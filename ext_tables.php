<?php

use TYPO3\CMS\Core\DataHandling\PageDoktypeRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3') || die('Access denied.');

call_user_func(
    function ($extKey) {
        //
        // Registers a Backend Module
        if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Questions',
                'web',
                'questions',
                '',
                [
                    \CodingMs\Questions\Controller\BackendController::class => 'overviewQuestions, overviewCategories',
                ],
                [
                    'access' => 'user,group',
                    'icon' => 'EXT:' . $extKey . '/Resources/Public/Icons/module-questions.svg',
                    'iconIdentifier' => 'module-questions',
                    'labels' => 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_questions.xlf',
                ]
            );
            $GLOBALS['PAGES_TYPES'][1659725048] = [
                'type' => 'web',
                // ATTENTION: Don't insert line breaks into the "allowedTables" - this will break this functionality!
                'allowedTables' => implode(
                    ',',
                    [
                        'tx_questions_domain_model_question',
                        'tx_questions_domain_model_questioncategory',
                        'sys_file_reference',
                        'pages',
                    ]
                ),
            ];
        } else {
            // Page type for v12
            $dokTypeRegistry = GeneralUtility::makeInstance(PageDoktypeRegistry::class);
            $dokTypeRegistry->add(
                1659725048,
                [
                    'type' => 'web',
                    'allowedTables' => implode(
                        ',',
                        [
                            'tx_questions_domain_model_question',
                            'tx_questions_domain_model_questioncategory',
                            'sys_file_reference',
                            'pages',
                        ]
                    ),
                ]
            );
        }
        //
        // register svg icons: identifier and filename
        $iconsSvg = [
            'module-questions' => 'Resources/Public/Icons/module-questions.svg',
            'apps-pagetree-questions' => 'Resources/Public/Icons/iconmonstr-help-3.svg',
            'content-plugin-questions-questions' => 'Resources/Public/Icons/Extension.svg',
        ];
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        foreach ($iconsSvg as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:' . $extKey . '/' . $path]
            );
        }
        //
        // Authorizations
        $GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['modules_question'] =
            \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
                \CodingMs\Questions\Domain\DataTransferObject\QuestionActionPermission::class
            );
        $GLOBALS['TYPO3_CONF_VARS']['BE']['customPermOptions']['modules_question_category'] =
            \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
                \CodingMs\Questions\Domain\DataTransferObject\QuestionCategoryActionPermission::class
            );
    },
    'questions'
);
