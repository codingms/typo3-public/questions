# TypoScript constant settings


## Questions Container

| **Constant**      | themes.configuration.container.questions                       |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Container for question records                                 |
| **Description**   |                                                                |
| **Type**          | int+                                                           |
| **Default value** | 0                                                              |



## Questions Pages

| **Constant**      | themes.configuration.pages.questions.list                      |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Page uid for default questions list                            |
| **Description**   |                                                                |
| **Type**          | int+                                                           |
| **Default value** | 0                                                              |

| **Constant**      | themes.configuration.pages.questions.detail                    |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Page uid for default question detail view                      |
| **Description**   |                                                                |
| **Type**          | int+                                                           |
| **Default value** | 0                                                              |



## Fluid Templates for questions

| **Constant**      | themes.configuration.extension.questions.view.templateRootPath |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Fluid Templates                                                |
| **Description**   |                                                                |
| **Type**          | string                                                         |
| **Default value** | EXT:questions/Resources/Private/Templates/                     |

| **Constant**      | themes.configuration.extension.questions.view.partialRootPath  |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Fluid Partials                                                 |
| **Description**   |                                                                |
| **Type**          | string                                                         |
| **Default value** | EXT:questions/Resources/Private/Partials/                      |

| **Constant**      | themes.configuration.extension.questions.view.layoutRootPath   |
| :---------------- | :------------------------------------------------------------- |
| **Label**         | Fluid Layouts                                                  |
| **Description**   |                                                                |
| **Type**          | string                                                         |
| **Default value** | EXT:questions/Resources/Private/Layouts/                       |



