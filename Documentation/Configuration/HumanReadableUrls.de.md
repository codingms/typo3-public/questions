# Sprechende URLs

## Slug-Konfiguration (ab TYPO3 9.5)

```yaml
routeEnhancers:
  QuestionsPlugin:
    type: Extbase
    extension: Questions
    plugin: Questions
    routes:
      -
        routePath: '/{question_question}'
        _controller: 'Question::show'
        _arguments:
          question_question: question
    defaultController: 'Question::list'
    aspects:
      question_question:
        type: PersistedAliasMapper
        tableName: tx_questions_domain_model_question
        routeFieldName: slug
        routeValuePrefix: /
```


## Realurl-Konfiguration (bis TYPO3 9.5)

```php
$configuration = [

    'fixedPostVars' => [
        'questionsConfiguration' => [
            0 => [
                'GETvar' => 'tx_questions_questions[action]',
                'valueMap' => [],
                'noMatch' => 'bypass',
            ],
            1 => [
                'GETvar' => 'tx_questions_questions[controller]',
                'valueMap' => [],
                'noMatch' => 'bypass',
            ],
            2 => [
                'GETvar' => 'tx_questions_questions[question]',
                'lookUpTable' => [
                    'table' => 'tx_questions_domain_model_question',
                    'id_field' => 'uid',
                    //'alias_field' => 'question',
                    'alias_field' => "CONCAT(question, '-', uid)",
                    'addWhereClause' => ' AND NOT deleted',
                    'useUniqueCache' => 1,
                    'useUniqueCache_conf' => [
                        'strtolower' => 1,
                        'spaceCharacter' => '-',
                    ],
                    'languageGetVar' => 'L',
                    'languageExceptionUids' => '',
                    'languageField' => 'sys_language_uid',
                    'transOrigPointerField' => 'l10n_parent',
                    'autoUpdate' => 1,
                    'expireDays' => 180,
                ],
            ],
        ],
        62 => 'questionsConfiguration',
    ],
];
```
