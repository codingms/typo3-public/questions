# Sitemap.xml Konfiguration für Fragen

Mit der folgenden Konfiguration kannst Du eine separate Sitemap.xml für die Fragen einrichten:

```typo3_typoscript
plugin.tx_seo {
	config {
		xmlSitemap {
			sitemaps {
				questions {
					provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
					config {
						table = tx_questions_domain_model_question
						sortField = sorting
						lastModifiedField = tstamp
						recursive = 1
						pid = 769
						url {
							pageId = 2014
							fieldToParameterMap {
								uid = tx_questions_questions[question]
							}
							additionalGetParameters {
								tx_questions_questions.controller = Question
								tx_questions_questions.action = show
							}
							useCacheHash = 1
						}
					}
				}
			}
		}
	}
}
```
