# Sitemap.xml configuration for questions

The following configuration enables you to generate a Sitemap.xml for your questions:

```typo3_typoscript
plugin.tx_seo {
	config {
		xmlSitemap {
			sitemaps {
				questions {
					provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
					config {
						table = tx_questions_domain_model_question
						sortField = sorting
						lastModifiedField = tstamp
						recursive = 1
						pid = 769
						url {
							pageId = 2014
							fieldToParameterMap {
								uid = tx_questions_questions[question]
							}
							additionalGetParameters {
								tx_questions_questions.controller = Question
								tx_questions_questions.action = show
							}
							useCacheHash = 1
						}
					}
				}
			}
		}
	}
}
```
