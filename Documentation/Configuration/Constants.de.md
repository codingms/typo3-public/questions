# TypoScript-Konstanten Einstellungen


## Fragen Container

| **Konstante**    | themes.configuration.container.questions                       |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | Container für Fragendatensätze                                 |
| **Beschreibung** |                                                                |
| **Typ**          | int+                                                           |
| **Standardwert** | 0                                                              |



## Questions Seiten

| **Konstante**    | themes.configuration.pages.questions.list                      |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | Page-UID für Standard-Fragenliste                              |
| **Beschreibung** |                                                                |
| **Typ**          | int+                                                           |
| **Standardwert** | 0                                                              |

| **Konstante**    | themes.configuration.pages.questions.detail                    |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | Page-UID für die Detailansicht der Standardfrage               |
| **Beschreibung** |                                                                |
| **Typ**          | int+                                                           |
| **Standardwert** | 0                                                              |



## Fluid-Templates für Fragen

| **Konstante**    | themes.configuration.extension.questions.view.templateRootPath |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | Fluid Templates                                                |
| **Beschreibung** |                                                                |
| **Typ**          | string                                                         |
| **Standardwert** | EXT:questions/Resources/Private/Templates/                     |

| **Konstante**    | themes.configuration.extension.questions.view.partialRootPath  |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | Fluid Partials                                                 |
| **Beschreibung** |                                                                |
| **Typ**          | string                                                         |
| **Standardwert** | EXT:questions/Resources/Private/Partials/                      |

| **Konstante**    | themes.configuration.extension.questions.view.layoutRootPath   |
|:-----------------|:---------------------------------------------------------------|
| **Label**        | Fluid Layouts                                                  |
| **Beschreibung** |                                                                |
| **Typ**          | string                                                         |
| **Standardwert** | EXT:questions/Resources/Private/Layouts/                       |



