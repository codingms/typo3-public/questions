# Wie können die default Meta tags in der Detailansicht deaktiviert werden?

Einfach folgendes im Root-Template:

```typo3_typoscript
# Remove meta-Tags
[globalVar = GP:tx_questions_questions|question > 0]
	page.meta {
		abstract >
		keywords >
		description >
	}
[global]
```

Besser noch, Du nimmst die Condition weg und platzierst das TypoScript in einem Extension-Template direkt auf der Detail-Seite.
So hast Du zwar ein Template-Record mehr, aber eine Condition weniger die im Root-Template verarbeitet werden muss.
