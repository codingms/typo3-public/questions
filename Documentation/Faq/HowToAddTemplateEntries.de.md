# Wie füge ich Template Auswahl Einträge hinzu?

... einfach folgendes im Page Typoscript:

```typo3_typoscript
TCEFORM.tx_questions_domain_model_question {
	template {
		addItems {
			HeaderReadLargeText = Header-Rot, Schrift groß
			HeaderGrayLargeText = Header-Grau, Schrift groß
			NormalWithoutImage = Normal ohne Bild
			NormalWithImageRight = Normal mit Bild rechts
			NormalWithoutImageColorsInverted = Normal ohne Bild, Farben invertiert
			NormalWithoutHeadlineImageTop = Normal ohne Headline, Bild oben
		}
	}
}
```
