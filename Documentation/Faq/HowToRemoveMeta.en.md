# How do I deactivate the default meta tags in the detail view?

Just add the following to the root template:

```typo3_typoscript
# Remove meta-Tags
[globalVar = GP:tx_questions_questions|question > 0]
	page.meta {
		abstract >
		keywords >
		description >
	}
[global]
```

Better still, remove the condition and place the Typoscript directly in the extension template on the detail page. You will have one more template record but there will be one less condition to maintain in the root template. 
