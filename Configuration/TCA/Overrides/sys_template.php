<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

//
// Static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'questions',
    'Configuration/TypoScript',
    'Questions - Frequently asked questions (FAQ)'
);
