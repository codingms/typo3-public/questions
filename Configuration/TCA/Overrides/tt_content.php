<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

//
// Plugins
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Questions',
    'Questions',
    'FAQs'
);
//
// Include flex forms
$flexForms = ['Questions'];
foreach ($flexForms as $pluginName) {
    $extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('questions');
    $pluginSignature = strtolower($extensionName) . '_' . strtolower($pluginName);
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
    $flexForm = 'FILE:EXT:questions/Configuration/FlexForms/' . $pluginName . '.xml';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, $flexForm);
}
