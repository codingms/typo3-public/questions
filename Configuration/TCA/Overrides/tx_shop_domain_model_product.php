<?php

defined('TYPO3') or die();

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('shop')) {
    /**
     * Add questions to shop products
     */
    $newColumns = [
        'question_categories' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_product.question_categories',
            'description' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_product.question_categories_description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_questions_domain_model_questioncategory',
                'MM' => 'tx_shop_product_productquestioncategories_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'tx_questions_domain_model_questioncategory',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tx_shop_domain_model_product',
        $newColumns
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_shop_domain_model_product',
        'question_categories',
        '',
        'after:related_products'
    );
}
