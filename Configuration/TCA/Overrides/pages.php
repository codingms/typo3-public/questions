<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

// Add new page type as possible select item:


if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'pages',
        'doktype',
        [
            'LLL:EXT:questions/Resources/Private/Language/locallang_db.xlf:tx_questions_label.questions_page_type',
            1659725048,
            'EXT:questions/Resources/Public/Icons/iconmonstr-help-3.svg',
            'special'
        ],
        '1',
        'after'
    );
} else {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'pages',
        'doktype',
        [
            'label' => 'LLL:EXT:questions/Resources/Private/Language/locallang_db.xlf:tx_questions_label.questions_page_type',
            'value' => 1659725048,
            'icon' => 'EXT:questions/Resources/Public/Icons/iconmonstr-help-3.svg',
            'group' => 'special'
        ],
        '1',
        'after'
    );
}

\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
    $GLOBALS['TCA']['pages'],
    [
        // add icon for new page type:
        'ctrl' => [
            'typeicon_classes' => [
                1659725048 => 'apps-pagetree-questions',
            ],
        ],
        // add all page standard fields and tabs to your new page type
        'types' => [
            1659725048 => [
                'showitem' => '
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;;standard,
                    --palette--;;titleonly,
                    hidden
                '
            ]
        ]
    ]
);
