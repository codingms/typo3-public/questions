<?php

$extKey = 'questions';
$table = 'tx_questions_domain_model_question';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title'	=> $lll,
        'label' => 'question',
        'label_alt' => 'category',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'question,answer,markdown,category',
        'iconfile' => 'EXT:questions/Resources/Public/Icons/iconmonstr-help-3.svg',
        'accessUtility' => CodingMs\Questions\Utility\AccessUtility::class,
    ],
    'types' => [
        '1' => ['showitem' => '
            information,
            question,
            slug,
            template,
            answer,
            attribute1,
            attribute2,
            attribute3,
            attribute4,
        --div--;' . $lll . '.tab_category,
            category,
        --div--;' . $lll . '.tab_images,
            images,
        --div--;' . $lll . '.tab_seo,
            html_title,
            meta_abstract,
            meta_description,
            meta_keywords,
        --div--;' . $lll . '.tab_markdown,
            markdown,
        --div--;' . \CodingMs\Questions\Tca\Configuration::label('tab_language') . ',
            sys_language_uid,
            l10n_parent,
            l10n_diffsource,
        --div--;' . \CodingMs\Questions\Tca\Configuration::label('tab_access') . ',
            hidden,
            starttime,
            endtime
        '],
    ],
    /**
     * @todo add a type for switching between html and markdown!!
     */
    'columns' => [
        'information' => \CodingMs\Questions\Tca\Configuration::full('information', '', $extKey),
        'sys_language_uid' => \CodingMs\Questions\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\Questions\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\Questions\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\Questions\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\Questions\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\Questions\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\Questions\Tca\Configuration::full('endtime'),
        'slug' => [
            'exclude' => 0,
            'label' => $lll . '.slug',
            'config' => \CodingMs\Questions\Tca\Configuration::get('slug', false, false, '', ['field' => 'question']),
        ],
        'question' => [
            'exclude' => 0,
            'label' => $lll . '.question',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string', true),
        ],
        'template' => [
            'exclude' => false,
            'label' => $lll . '.template',
            'config' => \CodingMs\Questions\Tca\Configuration::get('select', true, false, '', [
                ['label' => $lll . '.template_default', 'value' => 'Default'],
                ['label' => $lll . '.template_with_single_view', 'value' => 'WithSingleView'],
            ]),
        ],
        'answer' => [
            'exclude' => 0,
            'label' => $lll . '.answer',
            'config' => \CodingMs\Questions\Tca\Configuration::get('rte', true),
            'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
        ],
        'category' => [
            'exclude' => 0,
            'label' => $lll . '.category',
            'description' => $lll . '.categories_description',
            'config' => \CodingMs\Questions\Tca\Configuration::get('questionCategories'),
        ],
        'images' => [
            'exclude' => false,
            'label' => $lll . '.images',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('images', false, false, '', ['field' => 'images']),
        ],
        'markdown' => [
            'exclude' => 0,
            'label' => $lll . '.markdown',
            'config' => \CodingMs\Questions\Tca\Configuration::get('markdown'),
            'defaultExtras' => 'fixed-font:enable-tab',
        ],
        'attribute1' => [
            'exclude' => 0,
            'label' => $lll . '.attribute1',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string'),
        ],
        'attribute2' => [
            'exclude' => 0,
            'label' => $lll . '.attribute2',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string'),
        ],
        'attribute3' => [
            'exclude' => 0,
            'label' => $lll . '.attribute3',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string'),
        ],
        'attribute4' => [
            'exclude' => 0,
            'label' => $lll . '.attribute4',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string'),
        ],
        'html_title' => [
            'exclude' => 0,
            'label' => $lll . '.html_title',
            'description' => $lll . '.html_title_description',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string'),
        ],
        'meta_abstract' => [
            'exclude' => 0,
            'label' => $lll . '.meta_abstract',
            'description' => $lll . '.meta_abstract_description',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string'),
        ],
        'meta_description' => [
            'exclude' => 0,
            'label' => $lll . '.meta_description',
            'description' => $lll . '.meta_description_description',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string'),
        ],
        'meta_keywords' => [
            'exclude' => 0,
            'label' => $lll . '.meta_keywords',
            'description' => $lll . '.meta_keywords_description',
            'config' => \CodingMs\Questions\Tca\Configuration::get('string'),
        ],
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
    $return['columns']['template']['config']['items'] = [
        [$lll . '.template_default', 'Default'],
        [$lll . '.template_with_single_view', 'WithSingleView'],
    ];
}
return $return;
