<?php

$return['questions_questions'] = [
    'parent' => 'web',
    'position' => [],
    'access' => 'user',
    'iconIdentifier' => 'module-questions',
    'path' => '/module/questions/questions',
    'labels' => 'LLL:EXT:questions/Resources/Private/Language/locallang_questions.xlf',
    'extensionName' => 'Questions',
    'controllerActions' => [
        \CodingMs\Questions\Controller\BackendController::class => [
            'overviewQuestions',
            'overviewCategories',
        ]
    ],
];
return $return;
