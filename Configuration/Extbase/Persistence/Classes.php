<?php

declare(strict_types=1);

return [
    \CodingMs\Questions\Domain\Model\Question::class => [
        'tableName' => 'tx_questions_domain_model_question',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
            'creationUser' => [
                'fieldName' => 'cruser'
            ],
        ],
    ],
    \CodingMs\Questions\Domain\Model\QuestionCategory::class => [
        'tableName' => 'tx_questions_domain_model_questioncategory',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
            'disable' => [
                'fieldName' => 'disable'
            ],
        ],
    ],
];
