# Questions Erweiterung für TYPO3

Zeige und verwalte FAQ - SEO optimiert.


**Features:**

*   Einfach zu verwenden
*   SEO optimierte Integration
*   Microdaten (http://schema.org/Question)
*   Liste mit Suchwort- und Kategorie-Filter
*   Markdown-Unterstützung für Antworttext (erfordert EXT:parsedown_extra)
*   FAQ Erstellung für Shop-Produkte möglich (EXT:shop)
*   Zusätzliche Felder für individuelle Informationen
*   Backendmodul zur Verwaltung von Fragen und Antworten
*   CSV-Export der Datensätze im Backend
*   Lässt sich auch einfach in ein Intranet zur Anzeige von wiederkehrenden Fragen integrieren

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Questions Dokumentation](https://www.coding.ms/documentation/typo3-questions "Questions Dokumentation")
*   [Questions Bug-Tracker](https://gitlab.com/codingms/typo3-public/questions/-/issues "Questions Bug-Tracker")
*   [Questions Repository](https://gitlab.com/codingms/typo3-public/questions "Questions Repository")
*   [TYPO3 Questions Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-questions/ "TYPO3 Questions Produktdetails")
*   [TYPO3 Questions Dokumentation](https://www.coding.ms/de/dokumentation/typo3-questions "TYPO3 Questions Dokumentation")
*   [TYPO3 Questions Download](https://typo3.org/extensions/repository/view/questions "TYPO3 Questions Download")
*   [TYPO3 Parsedown-Extra Produktdetails](https://www.coding.ms/typo3-extensions/typo3-parsedown-extra/ "TYPO3 Parsedown-Extra Produktdetails")
*   [TYPO3 Shop Produktdetails](https://www.coding.ms/typo3-extensions/typo3-shop/ "TYPO3 Shop Produktdetails")
