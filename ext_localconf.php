<?php

defined('TYPO3') || die('Access denied.');

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Questions',
            'Questions',
            [\CodingMs\Questions\Controller\QuestionController::class => 'list, show'],
            [\CodingMs\Questions\Controller\QuestionController::class => '']
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            '@import "EXT:' . $extKey . '/Configuration/PageTS/tsconfig.typoscript">'
        );
        //
        // Backend TypoScript
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
            '@import "EXT:questions/Configuration/TypoScript/Backend/setup.typoscript"'
        );
        //
        // Page title
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(trim('
            config.pageTitleProviders {
                questions {
                    provider = CodingMs\Questions\PageTitle\PageTitleProvider
                    before = altPageTitle,record,seo
                }
            }
        '));
        //
        // Allow backend users to drag and drop the new page type:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
            'options.pageTree.doktypesToShowInNewPageDragArea := addToList(1659725048)'
        );
    },
    'questions'
);
