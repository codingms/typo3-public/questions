/**
 * Author: Thomas Deuling <typo3@coding.ms>
 */
var Questions = {

	questionsFilter: null,
	questionListContent: null,
	questionSearchWordField: null,
	questionCategorySelect: null,
	eventType: 'click',
	pagination: null,

	filter: {
		offset: 0,
		limit: 0,
		resultCount: 0
	},

	/**
	 * Initialize questions
	 */
	initialize: function() {
		//
		// Detect event type
		if(jQuery('html.touch').length === 1) {
			this.eventType = 'touchstart';
		}
		//
		// If JavaScript is available, show filter feature
		this.questionsFilter = jQuery('.questions-filter');
		if(this.questionsFilter.length > 0) {
			this.questionsFilter.slideDown(1000);
			this.questionSearchWordField = jQuery('#question-search-word', this.questionsFilter);
			this.questionSearchWordField.on('keyup', this.searchByFilter);
			this.questionCategorySelect = jQuery('#question-category', this.questionsFilter);
			this.questionCategorySelect.on('change', this.searchByFilter);
		}
		//
		// Question list content
		this.questionListContent = jQuery('.questions');
		if(this.questionListContent.length > 0) {
			Questions.filter.resultCount = jQuery('.question', this.questionListContent).length;
			//
			// Prepare pagination
			Questions.pagination = jQuery('.question-list-pagination');
			if(Questions.pagination.length > 0) {
				Questions.pagination.show();
				Questions.filter.limit = parseInt(jQuery('*[data-items-per-page]').data('items-per-page'), 10);
				if(Questions.filter.limit === 0) {
					alert('Please configure a valid limit for pagination!');
				}
			}
			Questions.searchByFilter();
		}
	},

	/**
	 * Search by filter
	 */
	searchByFilter: function(resetOffset) {
		//
		// Offset needs to be reset?
		if (typeof resetOffset === 'undefined') {
			resetOffset = true;
		}
		if(resetOffset) {
			Questions.filter.offset = 0;
		}
		//
		// Process search parameter
		if(Questions.questionsFilter.length > 0) {
			Questions.filter.searchWord = Questions.questionSearchWordField.val().toLowerCase();
			Questions.filter.category = Questions.questionCategorySelect.val();
			jQuery.each(jQuery('.question', Questions.questionListContent), function () {
				var question = jQuery(this);
				question.hide();
				//
				// Search by word
				var searchWordFound = true;
				var questionSearchWord = question.data('search-word').toLowerCase();
				if (Questions.filter.searchWord !== '') {
					searchWordFound = (questionSearchWord.indexOf(Questions.filter.searchWord) !== -1);
				}
				//
				// Search by category
				var categoryFound = true;
				var questionCategory = String(question.data('category'));
				var questionCategories = questionCategory.split(',');
				if (Questions.filter.category !== '') {
					jQuery.each(questionCategories, function (key, value) {
						if (value === Questions.filter.category) {
							categoryFound = true;
							return false;
						}
						categoryFound = false;
					});
				}
				// Show question?!
				if (searchWordFound && categoryFound) {
					question.show();
				}
			});
		}
		else {
			jQuery('.question', Questions.questionListContent).show();
		}
		//
		// Count and show result amount
		var count = jQuery('.question:visible', Questions.questionListContent).length;
		if(count === 0) {
			//
			// If there is no result, show all
			jQuery('.question', Questions.questionListContent).show();
			count = jQuery('.question:visible', Questions.questionListContent).length;
			jQuery('.question-list-message-no-result').slideDown(1000);
		}
		else {
			jQuery('.question-list-message-no-result').slideUp(1000);
		}
		//
		// Insert result amount
		jQuery('.question-list-item-count').html(count);
		if(count === 1) {
			jQuery('.question-list-item-count-singular').show();
			jQuery('.question-list-item-count-plural').hide();
		}
		else {
			jQuery('.question-list-item-count-singular').hide();
			jQuery('.question-list-item-count-plural').show();
		}
		//
		// Refresh pagination
		if(Questions.pagination.length > 0) {
			var questions = jQuery('.question:visible', Questions.questionListContent);
			Questions.filter.resultCount = questions.length;
			Questions.refreshPagination();
			var questionIndex = 1;
			jQuery.each(questions, function() {
				var question = jQuery(this);
				if(questionIndex > Questions.filter.offset && questionIndex <= Questions.filter.offset+Questions.filter.limit) {
					question.show();
				}
				else {
					question.hide();
				}
				questionIndex++;
			});
		}
	},

	/**
	 * Build pagination
	 */
	refreshPagination: function() {
		jQuery.each(jQuery('.question-list-pagination'), function() {
			var pagination = jQuery(this);
			var count = Questions.filter.resultCount;
			var limit = Questions.filter.limit;
			var offset = Questions.filter.offset;
			var pageCount = Math.ceil(count / limit);
			var currentPageNo = (offset / limit) + 1;
			//
			// Insert pageinformation
			jQuery('.current', Questions.pagination).html(currentPageNo);
			jQuery('.all', Questions.pagination).html(pageCount);
			//
			// Remove all page items
			jQuery('.page-item.clone', pagination).remove();
			//
			// Currently on first page
			var paginationPrevious = jQuery('.page-item.previous');
			if(currentPageNo === 1) {
				paginationPrevious.addClass('disabled');
			}
			else if(paginationPrevious.hasClass('disabled')) {
				paginationPrevious.removeClass('disabled');
			}
			if(!paginationPrevious.hasClass('initialized')) {
				paginationPrevious.addClass('initialized');
				paginationPrevious.on('click', function() {
					var limit = Questions.filter.limit;
					var offset = Questions.filter.offset;
					// Increase offset
					offset -= limit;
					if(offset >= 0) {
						Questions.filter.offset = offset;
						Questions.searchByFilter(false);
						Questions.scrollToTop();
					}
					return false;
				});
			}
			//
			// Currently on last page
			var paginationNext = jQuery('.page-item.next');
			if(currentPageNo === pageCount) {
				paginationNext.addClass('disabled');
			}
			else if(paginationNext.hasClass('disabled')) {
				paginationNext.removeClass('disabled');
			}
			if(!paginationNext.hasClass('initialized')) {
				paginationNext.addClass('initialized');
				paginationNext.on('click', function() {
					var count = Questions.filter.resultCount;
					var limit = Questions.filter.limit;
					var offset = Questions.filter.offset;
					// Increase offset
					offset += limit;
					if(offset < count) {
						Questions.filter.offset = offset;
						Questions.searchByFilter(false);
						Questions.scrollToTop();
					}
					return false;
				});
			}
			//
			// Build page links
			var pageNo = 1;
			for(var i = 0 ; i<count ; i+=limit) {
				var pageItemTemplate = jQuery('.page-item.template', pagination).clone();
				pageItemTemplate.removeClass('template').addClass('clone').removeAttr('style');
				jQuery('a', pageItemTemplate).attr('data-page-no', pageNo).text(pageNo);
				// is current page
				if(pageNo === currentPageNo) {
					pageItemTemplate.addClass('active');
				}
				jQuery(pageItemTemplate).insertBefore(jQuery('.page-item.template', pagination));
				pageNo++;
			}
			jQuery('.page-item.clone a', pagination).on('click', function() {
				var pageLink = jQuery(this);
				var pageNo = parseInt(pageLink.attr('data-page-no'), 10);
				var limit = Questions.filter.limit;
				Questions.filter.offset = limit * (pageNo - 1);
				Questions.searchByFilter(false);
				Questions.scrollToTop();
				return false;
			});
		});
	},

	scrollToTop: function() {
		var scrollToOffset = parseInt(jQuery('*[data-scroll-to-offset]').data('scroll-to-offset'), 10);
		jQuery('html, body').animate({
			scrollTop: jQuery('.tx-questions').offset().top + scrollToOffset
		}, 1000);
	}

};
jQuery(document).ready(function() {
	Questions.initialize();
});
